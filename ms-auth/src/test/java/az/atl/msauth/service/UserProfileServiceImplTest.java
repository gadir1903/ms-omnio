package az.atl.msauth.service;

import az.atl.msauth.dao.entity.UserEntity;
import az.atl.msauth.dao.repository.UserRepo;
import az.atl.msauth.exception.InvalidOldPasswordException;
import az.atl.msauth.exception.UserNotFoundException;
import az.atl.msauth.model.Role;
import az.atl.msauth.model.dto.UserProfileDto;
import az.atl.msauth.service.impl.UserProfileServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static az.atl.msauth.model.mapper.UserProfileMapper.PROFILE_MAPPER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class UserProfileServiceImplTest {
    @Mock
    private UserRepo userRepo;

    @Mock
    private MessageSource messageSource;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserProfileServiceImpl userProfileService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        userProfileService = new UserProfileServiceImpl(userRepo, passwordEncoder, messageSource);
    }

    @Test
    void testGetUserById_withTrueCase() {
        var userEntity = UserEntity.builder().id(1L).firstname("John").lastname("Doe").userName("john123").email("john@gmail.com").jobTitle("IT spec.").role(Role.ADMIN).build();
        var userDto = UserProfileDto.builder().id(1L).name("John").surname("Doe").userName("john123").email("john@gmail.com").jobTitle("IT spec.").role(Role.ADMIN).build();
        when(userRepo.findById(1L)).thenReturn(Optional.of(userEntity));

        var result = userProfileService.getUserProfileById(1L);

        assertEquals(userDto, result);
        assertEquals(userDto.getId(), result.getId());
    }

    @Test
    public void testUpdatePasswordWithValidData() {
        // Arrange
        Long userId = 1L;
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";
        String encodedNewPassword = "encodedNewPassword";

        Locale locale = Locale.US;
        when(messageSource.getMessage(eq("userNotFound"), any(Object[].class), eq(locale))).thenReturn("User not found message");
        when(messageSource.getMessage(eq("invalidOldPassword"), any(Object[].class), eq(locale))).thenReturn("Invalid old password message");

        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setPassword("encodedOldPassword");

        when(userRepo.findById(userId)).thenReturn(Optional.of(userEntity));
        when(passwordEncoder.matches(oldPassword, userEntity.getPassword())).thenReturn(true);
        when(passwordEncoder.encode(newPassword)).thenReturn(encodedNewPassword);

        userProfileService.updatePassword(userId, oldPassword, newPassword);

        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(1)).save(userEntity);

        assert (userEntity.getPassword().equals(encodedNewPassword));
    }

    @Test
    public void testUpdatePasswordWithInvalidOldPassword() {
        // Arrange
        Long userId = 1L;
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";

        Locale locale = Locale.US;
        when(messageSource.getMessage(eq("userNotFound"), any(Object[].class), eq(locale))).thenReturn("User not found message");
        when(messageSource.getMessage(eq("invalidOldPassword"), any(Object[].class), eq(locale))).thenReturn("Invalid old password message");

        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setPassword("encodedOldPassword");

        when(userRepo.findById(userId)).thenReturn(Optional.of(userEntity));
        when(passwordEncoder.matches(oldPassword, userEntity.getPassword())).thenReturn(false);

        // Act and Assert
        assertThrows(InvalidOldPasswordException.class, () -> {
            userProfileService.updatePassword(userId, oldPassword, newPassword);
        });

        // Verify that userRepo.save() was not called
        verify(userRepo, never()).save(userEntity);
    }

    @Test
    public void testUpdatePasswordWithUserNotFound() {
        // Arrange
        Long userId = 1L;
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";

        Locale locale = Locale.US;
        when(messageSource.getMessage(eq("userNotFound"), any(Object[].class), eq(locale))).thenReturn("User not found message");
        when(messageSource.getMessage(eq("invalidOldPassword"), any(Object[].class), eq(locale))).thenReturn("Invalid old password message");

        when(userRepo.findById(userId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(UserNotFoundException.class, () -> {
            userProfileService.updatePassword(userId, oldPassword, newPassword);
        });

        // Verify that userRepo.save() was not called
        verify(userRepo, never()).save(any(UserEntity.class));
    }

    @Test
    public void testGetAllUsers() {
        // Arrange
        UserProfileDto user1 = new UserProfileDto(1L, "John", "Doe", "doe", "john@gmail.com", "Junior Helpdesk", Role.USER);
        UserProfileDto user2 = new UserProfileDto(1L, "Chris", "Tucker", "chris", "chris@gmail.com", "Junior Helpdesk", Role.USER);

        when(userRepo.findAll()).thenReturn(Arrays.asList(PROFILE_MAPPER.buildDtoToEntity(user1), PROFILE_MAPPER.buildDtoToEntity(user2)));

        // Act
        List<UserProfileDto> result = userProfileService.getAllUsers();

        // Assert
        assertEquals(2, result.size());
        assertEquals(user1, result.get(0));
        assertEquals(user2, result.get(1));
    }

    @Test
    public void testUpdateUser() {
        // Arrange
        Long userId = 1L;
        UserProfileDto updatedProfile = new UserProfileDto(1L, "John", "Doe", "doe12345", "john@gmail.com", "Junior Helpdesk", Role.USER);

        UserEntity existingUser = new UserEntity(1L, "John", "Doe", "doe", "john@gmail.com", "Junior Helpdesk", "123456", Role.USER);

        when(userRepo.findById(userId)).thenReturn(Optional.of(existingUser));

        when(userRepo.save(existingUser)).thenReturn(existingUser);

        UserProfileDto result = userProfileService.updateUser(userId, updatedProfile);

        assertEquals(updatedProfile.getName(), existingUser.getFirstname());
        assertEquals(updatedProfile.getUserName(), existingUser.getUsername());
        assertEquals(updatedProfile.getEmail(), existingUser.getEmail());
        assertEquals(updatedProfile.getJobTitle(), existingUser.getJobTitle());
        assertEquals(updatedProfile.getRole(), existingUser.getRole());
        assertEquals(updatedProfile, result);
    }

    @Test
    public void testDeleteUserById() {
        Long userId = 1L;

        when(userRepo.existsById(userId)).thenReturn(true);

        userProfileService.deleteUserById(userId);

        verify(userRepo).deleteById(userId);
    }

    @Test
    public void testDeleteUserById_UserNotFound() {
        Long userId = 1L;

        when(userRepo.existsById(userId)).thenReturn(false);

        assertThrows(UserNotFoundException.class, () -> userProfileService.deleteUserById(userId));

        verify(userRepo, never()).deleteById(userId);
    }

    @Test
    public void testGetPaginatedUsers() {
        Pageable pageable = Pageable.ofSize(2).withPage(0);

        List<UserEntity> userEntities = Arrays.asList(
                new UserEntity(1L, "John", "Doe", "doe", "john@gmail.com", "Junior Helpdesk", "123456", Role.USER),
                new UserEntity(1L, "John", "Doe", "doe", "john@gmail.com", "Junior Helpdesk", "123456", Role.USER)
        );

        Page<UserEntity> page = new PageImpl<>(userEntities, pageable, userEntities.size());

        when(userRepo.findAll(pageable)).thenReturn(page);

        Page<UserProfileDto> result = userProfileService.getPaginatedUsers(pageable);

        assertEquals(2, result.getContent().size());
    }
}
