package az.atl.msauth.controller;

import az.atl.msauth.model.Role;
import az.atl.msauth.model.consts.UpdatePasswordRequest;
import az.atl.msauth.model.dto.UserProfileDto;
import az.atl.msauth.service.UserProfileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserProfileControllerTest {
    @InjectMocks
    private UserProfileController userProfileController;

    @Mock
    private UserProfileService userProfileService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testDeleteUserById() {
        Long userId = 1L;

        userProfileController.deleteUserById(userId);

        verify(userProfileService, times(1)).deleteUserById(userId);
    }

    @Test
    public void testUpdateUser() {
        Long userId = 1L;
        UserProfileDto updatedUserProfile = new UserProfileDto(1L, "John", "Doe", "doe12345", "john@gmail.com", "Junior Helpdesk", Role.USER);
        when(userProfileService.updateUser(userId, updatedUserProfile)).thenReturn(updatedUserProfile);

        UserProfileDto result = userProfileController.updateUser(userId, updatedUserProfile);

        verify(userProfileService, times(1)).updateUser(userId, updatedUserProfile);

        assertEquals(updatedUserProfile, result);
    }

    @Test
    public void testGetPaginatedUsers() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(1);
        List<UserProfileDto> userList = new ArrayList<>();
        userList.add(new UserProfileDto(1L, "John", "Doe", "doe12345", "john@gmail.com", "Junior Helpdesk", Role.USER));
        userList.add(new UserProfileDto(2L, "Jane", "Smith", "jane", "jane@gmail.com", "Middle Helpdesk", Role.USER));
        userList.add(new UserProfileDto(3L, "Bob", "Wilson", "bob123", "bob@gmail.com", "Senior Helpdesk", Role.ADMIN));
        when(userProfileService.getPaginatedUsers(pageable)).thenReturn(new PageImpl<>(userList));

        Page<UserProfileDto> result = userProfileController.getPaginatedUsers(pageable);

        verify(userProfileService, times(1)).getPaginatedUsers(pageable);

        assertEquals(userList, result.getContent());
    }

    @Test
    public void testGetAllUsers() {
        // Arrange
        List<UserProfileDto> userList = new ArrayList<>();
        userList.add(new UserProfileDto(1L, "John", "Doe", "doe12345", "john@gmail.com", "Junior Helpdesk", Role.USER));
        userList.add(new UserProfileDto(2L, "Jane", "Smith", "jane", "jane@gmail.com", "Middle Helpdesk", Role.USER));
        userList.add(new UserProfileDto(3L, "Bob", "Wilson", "bob123", "bob@gmail.com", "Senior Helpdesk", Role.ADMIN));


        when(userProfileService.getAllUsers()).thenReturn(userList);

        List<UserProfileDto> result = userProfileController.getAllUsers();

        verify(userProfileService, times(1)).getAllUsers();

        assertEquals(userList, result);
    }

    @Test
    public void testUpdatePassword() {
        // Arrange
        Long userId = 1L;
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";
        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest(oldPassword, newPassword);

        doNothing().when(userProfileService).updatePassword(userId, oldPassword, newPassword);

        ResponseEntity<String> responseEntity = userProfileController.updatePassword(userId, updatePasswordRequest);

        verify(userProfileService, times(1)).updatePassword(userId, oldPassword, newPassword);

        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
    }

    @Test
    public void testGetUserProfileById() {
        Long userId = 1L;
        UserProfileDto userProfileDto = new UserProfileDto(1L, "John", "Doe", "doe12345", "john@gmail.com", "Junior Helpdesk", Role.USER);

        when(userProfileService.getUserProfileById(userId)).thenReturn(userProfileDto);

        UserProfileDto result = userProfileController.getUserProfileById(userId);

        verify(userProfileService, times(1)).getUserProfileById(userId);

        assertEquals(userProfileDto, result);
    }
}
