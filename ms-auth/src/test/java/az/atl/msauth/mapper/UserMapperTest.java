package az.atl.msauth.mapper;

import az.atl.msauth.dao.entity.UserEntity;
import az.atl.msauth.model.Role;
import az.atl.msauth.model.dto.UserProfileDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static az.atl.msauth.model.mapper.UserProfileMapper.PROFILE_MAPPER;

public class UserMapperTest {
    @Test
    public void testMapEntityToDto() {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setFirstname("John");
        userEntity.setLastname("Doe");
        userEntity.setUserName("john123");
        userEntity.setJobTitle("IT");
        userEntity.setEmail("john@gmail.com");
        userEntity.setRole(Role.ADMIN);

        UserProfileDto userProfileDto = PROFILE_MAPPER.buildEntityToDto(userEntity);
        Assertions.assertEquals(userEntity.getId(), userProfileDto.getId());
        Assertions.assertEquals(userEntity.getFirstname(), userProfileDto.getName());
        Assertions.assertEquals(userEntity.getLastname(), userProfileDto.getSurname());
        Assertions.assertEquals(userEntity.getUsername(), userProfileDto.getUserName());
        Assertions.assertEquals(userEntity.getJobTitle(), userProfileDto.getJobTitle());
        Assertions.assertEquals(userEntity.getEmail(), userProfileDto.getEmail());
        Assertions.assertEquals(userEntity.getRole(), userProfileDto.getRole());
    }

    @Test
    public void testMapDtoEntity() {
        UserProfileDto userProfileDto = new UserProfileDto();
        userProfileDto.setId(1L);
        userProfileDto.setName("John");
        userProfileDto.setSurname("Doe");
        userProfileDto.setUserName("john123");
        userProfileDto.setJobTitle("IT");
        userProfileDto.setEmail("john@gmail.com");
        userProfileDto.setRole(Role.ADMIN);

        UserEntity userEntity = PROFILE_MAPPER.buildDtoToEntity(userProfileDto);
        Assertions.assertEquals(userProfileDto.getId(), userEntity.getId());
        Assertions.assertEquals(userProfileDto.getName(), userEntity.getFirstname());
        Assertions.assertEquals(userProfileDto.getSurname(), userEntity.getLastname());
        Assertions.assertEquals(userProfileDto.getUserName(), userEntity.getUsername());
        Assertions.assertEquals(userProfileDto.getJobTitle(), userEntity.getJobTitle());
        Assertions.assertEquals(userProfileDto.getEmail(), userEntity.getEmail());
        Assertions.assertEquals(userProfileDto.getRole(), userEntity.getRole());
    }
}
