package az.atl.msauth.service.impl;

import az.atl.msauth.dao.entity.UserEntity;
import az.atl.msauth.dao.repository.UserRepo;
import az.atl.msauth.exception.InvalidOldPasswordException;
import az.atl.msauth.exception.UserNotFoundException;
import az.atl.msauth.model.consts.UpdatePasswordRequest;
import az.atl.msauth.model.dto.UserProfileDto;
import az.atl.msauth.service.UserProfileService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

import static az.atl.msauth.model.mapper.UserProfileMapper.PROFILE_MAPPER;

@Service
@AllArgsConstructor
@Slf4j
public class UserProfileServiceImpl implements UserProfileService {
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    private final MessageSource messageSource;

    @Override
    public void updatePassword(Long userId, UpdatePasswordRequest updatePasswordRequest) {
        Locale locale = LocaleContextHolder.getLocale();
        Object[] args = new Object[1];
        String oldPassword = updatePasswordRequest.getOldPassword();
        String newPassword = updatePasswordRequest.getNewPassword();
        args[0] = oldPassword;
        String message = messageSource.getMessage("userNotFound", args, locale);
        UserEntity userEntity = userRepo.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(message));

        String storedEncodedPassword = userEntity.getPassword();
        if (!passwordEncoder.matches(oldPassword, storedEncodedPassword)) {
            String message1 = messageSource.getMessage("invalidOldPassword", args, locale);
            throw new InvalidOldPasswordException(message1);
        }

        String encodedNewPassword = passwordEncoder.encode(newPassword);
        userEntity.setPassword(encodedNewPassword);

        userRepo.save(userEntity);
    }

    @Override
    public UserProfileDto getUserProfileById(Long id) {
        Locale locale = LocaleContextHolder.getLocale();
        Object[] args = new Object[1];
        String message = messageSource.getMessage("userNotFound", args, locale);
        var userEntity = userRepo.findById(id).orElseThrow(() -> new UserNotFoundException(message));
        return PROFILE_MAPPER.buildEntityToDto(userEntity);
    }

    @Override
    public List<UserProfileDto> getAllUsers() {
        var userList = userRepo.findAll().stream().map(PROFILE_MAPPER::buildEntityToDto).toList();
        return userList;
    }

    @Override
    public UserProfileDto updateUser(Long id, UserProfileDto userProfileDto) {
        UserEntity user = userRepo.findById(id).get();
        user.setFirstname(userProfileDto.getName());
        user.setUserName(userProfileDto.getUserName());
        user.setEmail(userProfileDto.getEmail());
        user.setJobTitle(userProfileDto.getJobTitle());
        user.setRole(userProfileDto.getRole());
        userRepo.save(user);
        return PROFILE_MAPPER.buildEntityToDto(user);
    }

    @Override
    public void deleteUserById(Long id) {
        if (!userRepo.existsById(id)) {
            throw new UserNotFoundException("User with ID " + id + " not found.");
        }
        userRepo.deleteById(id);
    }

    @Override
    public Page<UserProfileDto> getPaginatedUsers(Pageable pageable) {
        return userRepo.findAll(pageable).map(PROFILE_MAPPER::buildEntityToDto);
    }
}
