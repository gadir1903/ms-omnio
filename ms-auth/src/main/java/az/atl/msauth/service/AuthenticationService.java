package az.atl.msauth.service;

import az.atl.msauth.dao.entity.UserEntity;
import az.atl.msauth.dao.repository.UserRepo;
import az.atl.msauth.exception.UserNotFoundException;
import az.atl.msauth.exception.UsernameAlreadyExist;
import az.atl.msauth.model.AuthenticationRequest;
import az.atl.msauth.model.AuthenticationResponse;
import az.atl.msauth.model.RegisterRequest;
import az.atl.msauth.model.RegisterResponse;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public record AuthenticationService(UserRepo userRepo,
                                    PasswordEncoder passwordEncoder,
                                    JwtService jwtService,
                                    AuthenticationManager authenticationManager,
                                    MessageSource messageSource) {
    public RegisterResponse register(RegisterRequest request) {
        var exist = userRepo.findByUserName(request.getUserName()).isPresent();
        if (exist) {
            Locale locale = LocaleContextHolder.getLocale();
            Object[] args = new Object[1];
            args[0] = request.getUserName();
            String message = messageSource.getMessage("usernameAlreadyExist", args, locale);
            throw new UsernameAlreadyExist(message);
        }
        var user = UserEntity.builder()
                .firstname(request.getFirstName())
                .lastname(request.getLastName())
                .userName(request.getUserName())
                .email(request.getEmail())
                .jobTitle(request.getJobTitle())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .build();
        var userEntity = userRepo.save(user);
        return RegisterResponse.buildRegisterDto(userEntity);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUserName(),
                        request.getPassword()
                )
        );
        Locale locale = LocaleContextHolder.getLocale();
        Object[] args = new Object[1];
        args[0] = request.getUserName();
        String message = messageSource.getMessage("userNotFound", args, locale);
        var user = userRepo.findByUserName(request.getUserName())
                .orElseThrow(() -> new UserNotFoundException(message));
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
