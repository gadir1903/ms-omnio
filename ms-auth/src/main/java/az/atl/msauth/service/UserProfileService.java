package az.atl.msauth.service;

import az.atl.msauth.model.consts.UpdatePasswordRequest;
import az.atl.msauth.model.dto.UserProfileDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserProfileService {
    void updatePassword(Long userId, UpdatePasswordRequest updatePasswordRequest);

    UserProfileDto getUserProfileById(Long id);

    List<UserProfileDto> getAllUsers();

    UserProfileDto updateUser(Long id, UserProfileDto userProfileDto);

    void deleteUserById(Long id);

    Page<UserProfileDto> getPaginatedUsers(Pageable pageable);
}
