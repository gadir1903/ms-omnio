package az.atl.msauth.service;

import az.atl.msauth.client.MessageClient;
import az.atl.msauth.model.SendMessageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageService {
    private final MessageClient messageClient;

    public void sendMessage(SendMessageRequest messageRequest) {
        log.info("message sent");
        messageClient.sendMessage(messageRequest);
    }
}
