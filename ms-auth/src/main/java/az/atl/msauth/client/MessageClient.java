package az.atl.msauth.client;

import az.atl.msauth.model.SendMessageRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "messageClient", url = "http://localhost:8889/messages")
public interface MessageClient {

    @PostMapping(value = "/send")
    void sendMessage(@RequestBody SendMessageRequest messageRequest);
}
