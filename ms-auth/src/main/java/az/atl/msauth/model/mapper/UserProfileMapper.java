package az.atl.msauth.model.mapper;

import az.atl.msauth.dao.entity.UserEntity;
import az.atl.msauth.model.dto.UserProfileDto;

public enum UserProfileMapper {
    PROFILE_MAPPER;

    public final UserEntity buildDtoToEntity(UserProfileDto userProfileDto) {
        return UserEntity.builder()
                .id(userProfileDto.getId())
                .firstname(userProfileDto.getName())
                .lastname(userProfileDto.getSurname())
                .userName(userProfileDto.getUserName())
                .email(userProfileDto.getEmail())
                .jobTitle(userProfileDto.getJobTitle())
                .role(userProfileDto.getRole())
                .build();
    }

    public final UserProfileDto buildEntityToDto(UserEntity userEntity) {
        return UserProfileDto.builder()
                .id(userEntity.getId())
                .name(userEntity.getFirstname())
                .surname(userEntity.getLastname())
                .userName(userEntity.getUsername())
                .email(userEntity.getEmail())
                .jobTitle(userEntity.getJobTitle())
                .role(userEntity.getRole())
                .build();
    }
}
