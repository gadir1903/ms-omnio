package az.atl.msauth.model;

public enum Role {
    ADMIN, USER
}
