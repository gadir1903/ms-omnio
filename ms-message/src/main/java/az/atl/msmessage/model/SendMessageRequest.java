package az.atl.msmessage.model;

import az.atl.msmessage.model.consts.Channels;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SendMessageRequest {

    Long recipientId;
    String content;
    Channels channels;
}
