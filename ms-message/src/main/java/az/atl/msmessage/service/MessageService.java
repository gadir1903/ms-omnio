package az.atl.msmessage.service;

import az.atl.msmessage.dao.entity.UserEntity;
import az.atl.msmessage.model.ReportResponse;
import az.atl.msmessage.model.SendMessageRequest;

import java.util.List;

public interface MessageService {
    void sendMessage(SendMessageRequest request, UserEntity sender);

    List<ReportResponse> getSentMessages(Long userId);

    List<ReportResponse> getReceivedMessages(Long userId);
}
