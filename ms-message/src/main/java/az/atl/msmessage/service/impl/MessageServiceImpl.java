package az.atl.msmessage.service.impl;

import az.atl.msmessage.dao.entity.MessageEntity;
import az.atl.msmessage.dao.entity.UserEntity;
import az.atl.msmessage.dao.repo.MessageRepository;
import az.atl.msmessage.dao.repo.UserRepo;
import az.atl.msmessage.exception.UserNotFoundException;
import az.atl.msmessage.model.ReportResponse;
import az.atl.msmessage.model.SendMessageRequest;
import az.atl.msmessage.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;
    private final UserRepo userRepo;

    @Override
    public void sendMessage(SendMessageRequest request, UserEntity sender) {
        UserEntity recipient = userRepo.findById(request.getRecipientId())
                .orElseThrow(() -> new UserNotFoundException("Recipient not found"));

        MessageEntity messageEntity = MessageEntity.builder()
                .sender(sender)
                .recipient(recipient)
                .content(request.getContent())
                .channels(request.getChannels())
                .createdAt(LocalDateTime.now())
                .build();
        messageRepository.save(messageEntity);
    }

    @Override
    public List<ReportResponse> getSentMessages(Long userId) {
        List<MessageEntity> sentMessages = messageRepository.findBySenderId(userId);
        return sentMessages.stream().map(messageEntity -> new ReportResponse(messageEntity.getRecipient().getId(), messageEntity.getContent())).collect(Collectors.toList());
    }

    @Override
    public List<ReportResponse> getReceivedMessages(Long userId) {
        List<MessageEntity> receivedMessages = messageRepository.findByRecipientId(userId);
        return receivedMessages.stream().map(messageEntity -> new ReportResponse(messageEntity.getSender().getId(), messageEntity.getContent())).collect(Collectors.toList());
    }
}