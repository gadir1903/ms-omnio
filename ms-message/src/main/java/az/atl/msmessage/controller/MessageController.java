package az.atl.msmessage.controller;

import az.atl.msmessage.dao.entity.UserEntity;
import az.atl.msmessage.dao.repo.UserRepo;
import az.atl.msmessage.exception.UserNotFoundException;
import az.atl.msmessage.model.ReportResponse;
import az.atl.msmessage.model.SendMessageRequest;
import az.atl.msmessage.service.MessageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/messages")
@AllArgsConstructor
@Slf4j
public class MessageController {
    private final MessageService messageService;
    private final UserRepo userRepo;

    @PostMapping("/send")
    public ResponseEntity<String> sendMessage(@RequestBody SendMessageRequest request, @AuthenticationPrincipal UserDetails userDetails) {

        UserEntity sender = userRepo.findByUserName(userDetails.getUsername()).orElseThrow(() -> new UserNotFoundException("Sender not found"));
        messageService.sendMessage(request, sender);
        return ResponseEntity.ok("Message sent successfully");
    }

    @GetMapping("/sent/{userId}")
    public ResponseEntity<List<ReportResponse>> getSentMessages(@PathVariable Long userId) {
        List<ReportResponse> sentMessages = messageService.getSentMessages(userId);
        return ResponseEntity.ok(sentMessages);
    }

    @GetMapping("/received/{userId}")
    public ResponseEntity<List<ReportResponse>> getReceivedMessages(@PathVariable Long userId) {
        List<ReportResponse> receivedMessages = messageService.getReceivedMessages(userId);
        return ResponseEntity.ok(receivedMessages);
    }
}
